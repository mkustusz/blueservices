package pl.kustusz.marek.blueservices.services;

import pl.kustusz.marek.blueservices.objects.InvestmentException;
import pl.kustusz.marek.blueservices.objects.funds.Fund;
import pl.kustusz.marek.blueservices.objects.investments.InvestmentType;
import pl.kustusz.marek.blueservices.objects.investments.Investment;

import java.math.BigDecimal;
import java.util.List;

/**
 * Service used  for investing
 */
public interface InvestService {

    /**
     *
     * @param funds list with funds for investing
     * @param amount amount of money to invest
     * @param investmentType type of investment
     * @return investment
     * @throws InvestmentException when there're any problems with initial data
     */
    Investment invest(List<Fund> funds, BigDecimal amount, InvestmentType investmentType) throws InvestmentException;
}
