package pl.kustusz.marek.blueservices.services.impl;

import org.springframework.stereotype.Service;
import pl.kustusz.marek.blueservices.objects.InvestmentException;
import pl.kustusz.marek.blueservices.objects.funds.FundType;
import pl.kustusz.marek.blueservices.objects.investments.InvestmentType;
import pl.kustusz.marek.blueservices.objects.funds.Fund;
import pl.kustusz.marek.blueservices.objects.funds.FundReturn;
import pl.kustusz.marek.blueservices.objects.investments.Investment;
import pl.kustusz.marek.blueservices.services.InvestService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class InvestServiceImpl implements InvestService {

    @Override
    public Investment invest(List<Fund> funds, BigDecimal amount, InvestmentType investmentType) throws InvestmentException {
        if (funds == null || funds.size() == 0) {
            throw  new InvestmentException("Invalid funds!");
        }
        if(amount == null || amount.compareTo(BigDecimal.ZERO) == 0 || amount.compareTo(BigDecimal.ZERO) == -1) {
            throw new InvestmentException("Invalid investment amount!");
        }
        if(investmentType == null) {
            throw new InvestmentException("Invalid investment type!");
        }
        Map<FundType, Long> numberOfFunds = funds.stream().collect(Collectors.toMap(
                Fund::getFundType, x->1L, Long::sum, () ->
                        EnumSet.allOf(FundType.class).stream().collect(Collectors.toMap(
                                x->x, x->0L, Long::sum, ()->new EnumMap<>(FundType.class)))));

        Map<FundType, BigDecimal> amounts = numberOfFunds.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        e -> amount.multiply(BigDecimal.valueOf(investmentType
                                .getPercentForFundType(e.getKey()))).setScale(0, BigDecimal.ROUND_DOWN)));

        BigDecimal amountsSum = amounts.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);

        List<FundReturn> fundReturns = funds.stream().map(f ->  {
            BigDecimal fundAmount = amounts.get(f.getFundType());
            Long number = numberOfFunds.get(f.getFundType());
            BigDecimal money = fundAmount.divide(BigDecimal.valueOf(number),
                    0, BigDecimal.ROUND_DOWN).add(fundAmount.remainder(BigDecimal.valueOf(number)));
            amounts.put(f.getFundType(), fundAmount.subtract(money));
            numberOfFunds.put(f.getFundType(), number - 1);
            return new FundReturn(f, money, money.divide(amountsSum).doubleValue());
        }).collect(Collectors.toList());

        return new Investment(fundReturns, amount.subtract(amountsSum));
    }
}
