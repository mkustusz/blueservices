package pl.kustusz.marek.blueservices.objects.funds;

/**
 * Represents polish fund and extends {@link Fund}
 */
public class PolishFund extends Fund {

    public PolishFund(Long id, String name) {
        super(id, name, FundType.POLISH);
    }
}
