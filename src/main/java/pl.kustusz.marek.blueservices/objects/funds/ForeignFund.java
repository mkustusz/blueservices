package pl.kustusz.marek.blueservices.objects.funds;

/**
 * Represents foreing fund and extends {@link Fund}
 */
public class ForeignFund extends Fund {
    public ForeignFund(Long id, String name) {
        super(id, name, FundType.FOREIGN);
    }
}
