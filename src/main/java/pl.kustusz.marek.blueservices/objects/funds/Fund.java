package pl.kustusz.marek.blueservices.objects.funds;

/**
 * Class that represents any fund
 */
public abstract class Fund {

    private Long id;
    private String name;
    private FundType fundType;

    public Fund(Long id, String name, FundType fundType) {
        this.id = id;
        this.name = name;
        this.fundType = fundType;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public FundType getFundType() {
        return fundType;
    }
}
