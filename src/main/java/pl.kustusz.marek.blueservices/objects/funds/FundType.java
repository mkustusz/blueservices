package pl.kustusz.marek.blueservices.objects.funds;

/**
 * Type of {@link Fund}
 */
public enum FundType {
    POLISH,
    FOREIGN,
    MONEY
}
