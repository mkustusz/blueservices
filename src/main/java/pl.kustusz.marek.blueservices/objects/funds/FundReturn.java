package pl.kustusz.marek.blueservices.objects.funds;

import java.math.BigDecimal;

/**
 * Represents calculated data for given fund
 */
public class FundReturn {

    private Fund fund;
    private BigDecimal amount;
    private Double percent;

    public FundReturn(Fund fund, BigDecimal amount, Double percent) {
        this.fund = fund;
        this.amount = amount;
        this.percent = percent;
    }

    public Fund getFund() {
        return fund;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Double getPercent() {
        return percent;
    }
}
