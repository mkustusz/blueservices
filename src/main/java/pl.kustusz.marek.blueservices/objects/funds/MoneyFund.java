package pl.kustusz.marek.blueservices.objects.funds;

/**
 * Represents money fund and extends {@link Fund}
 */
public class MoneyFund extends Fund {

    public MoneyFund(Long id, String name) {
        super(id, name, FundType.MONEY);
    }
}
