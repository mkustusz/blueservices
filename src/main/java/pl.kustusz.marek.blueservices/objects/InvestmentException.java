package pl.kustusz.marek.blueservices.objects;

/**
 * Created by kustma1 on 2017-07-21.
 */
public class InvestmentException extends Exception {

    public InvestmentException(String message) {
        super(message);
    }
}
