package pl.kustusz.marek.blueservices.objects.investments;

import pl.kustusz.marek.blueservices.objects.funds.FundReturn;

import java.math.BigDecimal;
import java.util.List;

/**
 * Finished investment
 */
public class Investment {

    private List<FundReturn> fundReturns;
    private BigDecimal returnAmount;

    public Investment(List<FundReturn> fundReturns, BigDecimal returnAmount) {
        this.fundReturns = fundReturns;
        this.returnAmount = returnAmount;
    }

    public List<FundReturn> getFundReturns() {
        return fundReturns;
    }

    public BigDecimal getReturnAmount() {
        return returnAmount;
    }
}
