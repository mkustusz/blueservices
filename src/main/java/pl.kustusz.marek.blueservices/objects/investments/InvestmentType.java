package pl.kustusz.marek.blueservices.objects.investments;

import pl.kustusz.marek.blueservices.objects.funds.FundType;

import java.util.HashMap;
import java.util.Map;

/**
 * Type of {@link Investment}
 */
public enum InvestmentType {
    SAFE("Safe", new HashMap<FundType, Double>(){{
        put(FundType.POLISH, 0.2);
        put(FundType.FOREIGN, 0.75);
        put(FundType.MONEY, 0.05);
    }}),
    BALANCED("Safe", new HashMap<FundType, Double>(){{
        put(FundType.POLISH, 0.3);
        put(FundType.FOREIGN, 0.6);
        put(FundType.MONEY, 0.1);
    }}),
    AGGRESIVE("Safe", new HashMap<FundType, Double>(){{
        put(FundType.POLISH, 0.4);
        put(FundType.FOREIGN, 0.2);
        put(FundType.MONEY, 0.4);
    }});

    private final String name;
    private Map<FundType, Double> funds;

    public String getName() {
        return name;
    }

    public Double getPercentForFundType(FundType fundType) {
        return funds.get(fundType);
    }

    InvestmentType(String name, Map<FundType, Double> funds) {
        this.name = name;
        this.funds = funds;
    }
}
