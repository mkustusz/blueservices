package pl.kustusz.marek.blueservices.services.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kustusz.marek.blueservices.objects.InvestmentException;
import pl.kustusz.marek.blueservices.objects.funds.FundType;
import pl.kustusz.marek.blueservices.objects.investments.InvestmentType;
import pl.kustusz.marek.blueservices.objects.funds.ForeignFund;
import pl.kustusz.marek.blueservices.objects.funds.Fund;
import pl.kustusz.marek.blueservices.objects.funds.MoneyFund;
import pl.kustusz.marek.blueservices.objects.funds.PolishFund;
import pl.kustusz.marek.blueservices.objects.funds.FundReturn;
import pl.kustusz.marek.blueservices.objects.investments.Investment;
import pl.kustusz.marek.blueservices.services.InvestService;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link InvestService}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InvestServiceImplTest {

    @Autowired
    InvestService investService;

    private List<Fund> funds;
    private List<Fund> funds2;

    @Before
    public void setUp() {
        funds = new LinkedList();
        funds.add(new PolishFund(1L, "Fundusz 1"));
        funds.add(new PolishFund(2L, "Fundusz 2"));
        funds.add(new ForeignFund(3L, "Fundusz 3"));
        funds.add(new ForeignFund(4L, "Fundusz 4"));
        funds.add(new ForeignFund(5L, "Fundusz 5"));
        funds.add(new MoneyFund(6L, "Fundusz 6"));

        funds2 = new LinkedList();
        funds2.add(new PolishFund(1L, "Fundusz 1"));
        funds2.add(new PolishFund(2L, "Fundusz 2"));
        funds2.add(new PolishFund(3L, "Fundusz 3"));
        funds2.add(new ForeignFund(4L, "Fundusz 4"));
        funds2.add(new ForeignFund(5L, "Fundusz 5"));
        funds2.add(new MoneyFund(6L, "Fundusz 6"));
    }

    @Test(expected = InvestmentException.class)
    public void shouldThrowExceptionForInvalidFunds() throws InvestmentException {
        investService.invest(null, new BigDecimal(10000), InvestmentType.SAFE);
    }

    @Test(expected = InvestmentException.class)
    public void shouldThrowExceptionForInvalidAmount() throws InvestmentException {
        investService.invest(funds, null, InvestmentType.SAFE);
    }

    @Test(expected = InvestmentException.class)
    public void shouldThrowExceptionForInvalidInvestmentType() throws InvestmentException {
        investService.invest(funds, new BigDecimal(10000), null);
    }

    @Test
    public void shouldPrepareForSafeInvestment() throws InvestmentException {
        Investment investment = investService.invest(funds, new BigDecimal(10000), InvestmentType.SAFE);
        assertEquals(investment.getFundReturns().size(), 6);
        assertEquals(investment.getReturnAmount().compareTo(BigDecimal.ZERO), 0);

        FundReturn fundReturn1 = investment.getFundReturns().get(0);
        assertEquals(fundReturn1.getFund().getFundType(), FundType.POLISH);
        assertEquals(fundReturn1.getAmount().compareTo(new BigDecimal(1000.0)), 0);
        assertEquals(fundReturn1.getPercent(), new Double(0.1));

        FundReturn fundReturn2 = investment.getFundReturns().get(1);
        assertEquals(fundReturn2.getFund().getFundType(), FundType.POLISH);
        assertEquals(fundReturn2.getAmount().compareTo(new BigDecimal(1000.0)), 0);
        assertEquals(fundReturn2.getPercent(), new Double(0.1));

        FundReturn fundReturn3 = investment.getFundReturns().get(2);
        assertEquals(fundReturn3.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn3.getAmount().compareTo(new BigDecimal(2500.0)), 0);
        assertEquals(fundReturn3.getPercent(), new Double(0.25));

        FundReturn fundReturn4 = investment.getFundReturns().get(3);
        assertEquals(fundReturn4.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn4.getAmount().compareTo(new BigDecimal(2500.0)), 0);
        assertEquals(fundReturn4.getPercent(), new Double(0.25));

        FundReturn fundReturn5 = investment.getFundReturns().get(4);
        assertEquals(fundReturn5.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn5.getAmount().compareTo(new BigDecimal(2500.0)), 0);
        assertEquals(fundReturn5.getPercent(), new Double(0.25));

        FundReturn fundReturn6 = investment.getFundReturns().get(5);
        assertEquals(fundReturn6.getFund().getFundType(), FundType.MONEY);
        assertEquals(fundReturn6.getAmount().compareTo(new BigDecimal(500.0)), 0);
        assertEquals(fundReturn6.getPercent(), new Double(0.05));
    }

    @Test
    public void shouldPrepareForSafeInvestmentWithReturn() throws InvestmentException {
        Investment investment = investService.invest(funds, new BigDecimal(10001), InvestmentType.SAFE);
        assertEquals(investment.getFundReturns().size(), 6);
        assertEquals(investment.getReturnAmount().compareTo(BigDecimal.ONE), 0);

        FundReturn fundReturn1 = investment.getFundReturns().get(0);
        assertEquals(fundReturn1.getFund().getFundType(), FundType.POLISH);
        assertEquals(fundReturn1.getAmount().compareTo(new BigDecimal(1000.0)), 0);
        assertEquals(fundReturn1.getPercent(), new Double(0.1));

        FundReturn fundReturn2 = investment.getFundReturns().get(1);
        assertEquals(fundReturn2.getFund().getFundType(), FundType.POLISH);
        assertEquals(fundReturn2.getAmount().compareTo(new BigDecimal(1000.0)), 0);
        assertEquals(fundReturn2.getPercent(), new Double(0.1));

        FundReturn fundReturn3 = investment.getFundReturns().get(2);
        assertEquals(fundReturn3.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn3.getAmount().compareTo(new BigDecimal(2500.0)), 0);
        assertEquals(fundReturn3.getPercent(), new Double(0.25));

        FundReturn fundReturn4 = investment.getFundReturns().get(3);
        assertEquals(fundReturn4.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn4.getAmount().compareTo(new BigDecimal(2500.0)), 0);
        assertEquals(fundReturn4.getPercent(), new Double(0.25));

        FundReturn fundReturn5 = investment.getFundReturns().get(4);
        assertEquals(fundReturn5.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn5.getAmount().compareTo(new BigDecimal(2500.0)), 0);
        assertEquals(fundReturn5.getPercent(), new Double(0.25));

        FundReturn fundReturn6 = investment.getFundReturns().get(5);
        assertEquals(fundReturn6.getFund().getFundType(), FundType.MONEY);
        assertEquals(fundReturn6.getAmount().compareTo(new BigDecimal(500.0)), 0);
        assertEquals(fundReturn6.getPercent(), new Double(0.05));
    }

    @Test
    public void shouldPrepareForSafeInvestmentWithoutReturn() throws InvestmentException {
        Investment investment = investService.invest(funds2, new BigDecimal(10000), InvestmentType.SAFE);
        assertEquals(investment.getFundReturns().size(), 6);
        assertEquals(investment.getReturnAmount().compareTo(BigDecimal.ZERO), 0);

        FundReturn fundReturn1 = investment.getFundReturns().get(0);
        assertEquals(fundReturn1.getFund().getFundType(), FundType.POLISH);
        assertEquals(fundReturn1.getAmount().compareTo(new BigDecimal(668.0)), 0);
        assertEquals(fundReturn1.getPercent(), new Double(0.0668));

        FundReturn fundReturn2 = investment.getFundReturns().get(1);
        assertEquals(fundReturn2.getFund().getFundType(), FundType.POLISH);
        assertEquals(fundReturn2.getAmount().compareTo(new BigDecimal(666.0)), 0);
        assertEquals(fundReturn2.getPercent(), new Double(0.0666));

        FundReturn fundReturn3 = investment.getFundReturns().get(2);
        assertEquals(fundReturn3.getFund().getFundType(), FundType.POLISH);
        assertEquals(fundReturn3.getAmount().compareTo(new BigDecimal(666.0)), 0);
        assertEquals(fundReturn3.getPercent(), new Double(0.0666));

        FundReturn fundReturn4 = investment.getFundReturns().get(3);
        assertEquals(fundReturn4.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn4.getAmount().compareTo(new BigDecimal(3750.0)), 0);
        assertEquals(fundReturn4.getPercent(), new Double(0.375));

        FundReturn fundReturn5 = investment.getFundReturns().get(4);
        assertEquals(fundReturn5.getFund().getFundType(), FundType.FOREIGN);
        assertEquals(fundReturn5.getAmount().compareTo(new BigDecimal(3750.0)), 0);
        assertEquals(fundReturn5.getPercent(), new Double(0.375));

        FundReturn fundReturn6 = investment.getFundReturns().get(5);
        assertEquals(fundReturn6.getFund().getFundType(), FundType.MONEY);
        assertEquals(fundReturn6.getAmount().compareTo(new BigDecimal(500.0)), 0);
        assertEquals(fundReturn6.getPercent(), new Double(0.05));
    }
}
