This is a simple application that can be used to simulate investments with various funds.

To build app please use gradle tasks:
gradle javaCompile - to build project
gradle test - to run tests

InvestService is the heart of whole application - inject it into your controller or use in it any other way.

Tests covers most cases.
